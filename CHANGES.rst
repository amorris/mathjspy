0.0.9 (unreleased)
==================

* Updated for Python3

0.0.8 (unreleased)
==================

* Fixed addition following subtraction
* Added a ? b : c syntax

0.0.7 (unreleased)
==================

* Override divide by zero errors for now.

0.0.6 (unreleased)
==================

* Moved depreciated ifElse function to ifElseAlt until a ? b : c syntax supported.

0.0.5 (unreleased)
==================

* Changed IRR function.

0.0.3 (unreleased)
==================

* Allow tuple / array data types for functions.

0.0.2 (unreleased)
==================

* Added log function

0.0.1 (unreleased)
==================

Initial Release
---------------

* Initial project released in Alpha.