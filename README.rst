mathjspy
========

mathjspy is a Python library evaluate mathematical expressions in the same manner as MathJS.

The purpose is to allow calculation of expressions in python that are consistent with what is calculated through
MathJS.

Many thanks to MathJS for their hard work in developing an excellent mathematical expression interpreter.
http://mathjs.org


Official documentation
======================

The homepage is http://mathjspy.readthedocs.org
You will find:

* every installation methods
* the official documentation
* code examples
* instructions for contributing
