This project is developed by Adam Morris.

A huge thanks to all the resources that contributed to this, especially to the contributors of mathjs for making an
excellent math expression interpreter in javascript:
http://mathjs.org

mathjspy is largely derived from Peter Norvig's work on lispy, found here:
http://norvig.com/lispy.html

And heavily influenced by Paul McGuire's work on fourFn.py found here:
http://pyparsing.wikispaces.com/file/view/fourFn.py

With further enhancements adapted from Sunjay Varma, Nathanial Peterson and others:
http://sunjay.ca/2014/04/27/evaluating-simple-math-expressions-using-python-and-regular-expressions
http://pyparsing.wikispaces.com/message/view/home/15549426
http://stackoverflow.com/questions/2371436/evaluating-a-mathematical-expression-in-a-string
