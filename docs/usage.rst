Simple usage
=======================

Evaluate an Expression
----------------------

MathJS.eval() is the main function for evaluating an expression.  It will change operators to functions, parse and
then evaluate::

    from mathjspy import MathJS

    mjs = MathJS()

    input_data = {'var_a': 5}
    mjs.update(input_data)              # Set variables used in equations from a dictionary
    mjs.set('var_b', 2)                 # Set individual variables
    mjs.eval('var_a + 5 * 4 / var_b')   # Evaluate an expression


MathJS.eval_map() will evaluate a series of expressions, recursively storing the results as re-usable variables::

    my_expression_map = [
                         ['var_a', '5 + 2'],
                         ['var_b', 'var_a + 3']
                        ]
    mjs.eval_map(my_expression_map)
    mjs.get('var_b')  # returns 10


